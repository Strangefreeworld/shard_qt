# shard_qt

A rewrite of Shard in PySide6

## ToDo

- [] Check for changes on save
- [X] Add non-Markdown processors
- [] Add TOML config (with reading/saving)
- [] Edit options
- [X] Line numbers
- [] Print to PDF
- [] Goto line