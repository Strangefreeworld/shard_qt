__version__ = "0.1.0"
__app_name__ = "shard"
__app_author__ = "strangefreeworld"

import sys
from pathlib import Path


def parent_dir():
    return str(Path(__file__).absolute().parents[0])


module_path = str(Path(__file__).absolute().parents[0])
if module_path not in sys.path:
    sys.path.append(module_path)
