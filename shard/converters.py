from collections import namedtuple
import mistune
import textile
from docutils.core import publish_string
from mau import Mau

Converter = namedtuple("Converter", "name function")


def textile_convert(text):
    """This function converts Textile text into HTML"""
    return textile.textile(text)


def rst_convert(text):
    """This function converts reStructuredText into HTML"""
    return publish_string(text, writer_name="html")


def simple_markdown(text):
    return mistune.html(text)


class MauConverter:
    def __init__(self):
        self._mau = Mau({}, "html")

    def __call__(self, text):
        return self._mau.process(text)


CONVERTERS = [
    {"name": "Markdown", "extensions": [".md"], "converter": simple_markdown},
    {"name": "Textile", "extensions": [".textile"], "converter": textile_convert},
    {"name": "reStructured Text", "extensions": [".rst"], "converter": rst_convert},
    {"name": "Mau", "extensions": [".mau"], "converter": MauConverter()},
]


def get_converter(extension):
    for item in CONVERTERS:
        if extension in item["extensions"]:
            return Converter(item["name"], item["converter"])
    return None


def converter_data(extension):
    data = get_converter(extension)
    return data if data else get_converter(".md")


def get_function(text_format):
    for item in CONVERTERS:
        if item["name"] == text_format:
            return item["converter"]
    return None


def converter_function(text_format=None):
    function = get_function(text_format)
    return function if function else get_function("Markdown")
