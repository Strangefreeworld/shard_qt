from pathlib import Path
from PySide6.QtWidgets import QSplitter
from PySide6.QtWebEngineWidgets import QWebEngineView
from QCodeEditor import CodeEditor
from converters import converter_function


class PreviewFrame(QSplitter):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.textedit = CodeEditor()
        self.htmlview = QWebEngineView()
        self.addWidget(self.textedit)
        self.addWidget(self.htmlview)
        self._file_path = None
        self._modified = False
        window_width = self.parent.frameGeometry().width()
        text_size = (window_width - 10)/2
        self.setSizes([text_size, text_size])
        self.textedit.textChanged.connect(self._translate_text)
        self._convert = converter_function()

    def _translate_text(self):
        text = self.textedit.toPlainText()
        self.htmlview.setHtml(self._convert(text))
        self._modified = True

    def open_file(self, path):
        try:
            with open(path) as read_file:
                file_text = read_file.read()
        except IOError as error:
            self.parent.critical_dialog(str(error))
        else:
            self._file_path = path
            self.textedit.setPlainText(file_text)

    def save(self, path=None):
        if path is None:
            if self._file_path is None:
                raise FileNotFoundError
        else:
            self._file_path = path
        with open(self._file_path, "w") as write_file:
            write_file.write(self.textedit.toPlainText())
            self._modified = False

    def paste(self):
        self.textedit.paste()

    def copy(self):
        self.textedit.copy()

    def cut(self):
        self.textedit.cut()

    @property
    def filename(self):
        return self._file_path

    @property
    def title(self):
        if not self._file_path:
            return ""
        return Path(self._file_path).name

    @property
    def modified(self):
        return self._modified
