from PySide6.QtWidgets import QMainWindow, QVBoxLayout, QMessageBox, QFileDialog
from PySide6.QtCore import Slot
from PySide6.QtGui import QAction, QIcon, QKeySequence, QCloseEvent
from shard_frame import PreviewFrame
from shard import parent_dir
from tomlkit import loads


class ShardWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Shard")
        self.setMinimumWidth(600)
        self.setMinimumHeight(600)
        self.window_layout = QVBoxLayout()
        file_menu = self.menuBar().addMenu("&File")
        icon = QIcon.fromTheme("document-open")
        open_action = QAction(icon, "Open File...", self, shortcut=QKeySequence.Open, triggered=self.open_file)
        file_menu.addAction(open_action)
        icon = QIcon.fromTheme("document-save")
        save_action = QAction(icon, "Save File", self, shortcut=QKeySequence.Save, triggered=self.save_file)
        file_menu.addAction(save_action)
        save_as_action = QAction(icon, "Save File As...", self, triggered=self.save_as)
        file_menu.addAction(save_as_action)
        icon = QIcon.fromTheme("application-exit")
        exit_action = QAction(icon, "Exit", self, triggered=self.close)
        file_menu.addAction(exit_action)
        icon = QIcon.fromTheme("edit-copy")
        edit_menu = self.menuBar().addMenu("&Edit")
        copy_action = QAction(icon, "Copy", self, shortcut=QKeySequence.Copy, triggered=self.copy)
        edit_menu.addAction(copy_action)
        icon = QIcon.fromTheme("edit-cut")
        cut_action = QAction(icon, "Cut", self, shortcut=QKeySequence.Cut, triggered=self.cut)
        edit_menu.addAction(cut_action)
        icon = QIcon.fromTheme("edit-paste")
        paste_action = QAction(icon, "Paste", self, shortcut=QKeySequence.Paste, triggered=self.paste)
        edit_menu.addAction(paste_action)

        self._main_frame = PreviewFrame(self)
        self.setCentralWidget(self._main_frame)
        tomlfile = f"{parent_dir()}/shard.toml"
        print(tomlfile)
        with open(tomlfile) as configfile:
            self._config = loads(configfile.read())

    @Slot()
    def open_file(self):
        input_file, _ = QFileDialog.getOpenFileName(self, "Open file", "", "All files(*.*)")
        if input_file:
            self._main_frame.open_file(input_file)

    @Slot()
    def save_file(self):
        if self._main_frame.filename:
            self._main_frame.save()
        else:
            save_file, _ = QFileDialog.getSaveFileName(self,
                                                       "Save File",
                                                       self._main_frame.title,
                                                       "All files(*.*)")
            if save_file:
                self._main_frame.save(save_file)

    @Slot()
    def save_as(self):
        save_file, _ = QFileDialog.getSaveFileName(self, "Save File", "", "All files(*.*)")
        if save_file:
            self._main_frame.save_file(save_file)

    def critical_dialog(self, message):
        dialog = QMessageBox(self)
        dialog.setText(message)
        dialog.setIcon(QMessageBox.Critical)
        dialog.show()

    def copy(self):
        self._main_frame.copy()

    def cut(self):
        self._main_frame.cut()

    def paste(self):
        self._main_frame.paste()

    def closeEvent(self, event: QCloseEvent) -> None:
        if self._main_frame.modified:
            response = QMessageBox.question(self,
                                            "Save Changes?",
                                            "Do you want to save changes to file?",
                                            buttons=QMessageBox.StandardButton.Yes|QMessageBox.StandardButton.No|QMessageBox.StandardButton.Cancel)
            if response == QMessageBox.StandardButton.Cancel:
                event.ignore()
                return
            if response == QMessageBox.StandardButton.Yes:
                self.save_file()
        event.accept()
