import shutil
from pathlib import Path
from shard import __app_name__
import click


def config_dir():
    return click.get_app_dir(__app_name__)


def check_config_dir():
    config_path = Path(config_dir())
    if not config_path.exists():
        config_path.mkdir(parents=True)


def config_file_path(file_name):
    config_path = Path(config_dir())
    return str(config_path / file_name)


def config_dir_files(file_list):
    check_config_dir()
    current_path = Path(__file__).absolute().parents[0]
    config_path = Path(config_dir())
    for item in file_list:
        config_dir_file = config_path / item
        if not config_dir_file.exists():
            shutil.copyfile(str(current_path / item), config_dir_file)
