import sys
from PySide6.QtWidgets import QApplication
from shard_window import ShardWindow


def run_shard():
    app = QApplication(sys.argv)
    window = ShardWindow()
    window.show()
    sys.exit(app.exec())